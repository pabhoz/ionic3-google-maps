import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  user: any;
  desde: any;
  hasta: any;
  fixcenter :boolean = true;

  constructor(public navCtrl: NavController, public geolocation: Geolocation) {}

  ionViewDidLoad(){
    //this.loadMap();
    this.findMe();
  }
 
  loadMap(){
 
    //let latLng = new google.maps.LatLng(3.3441473, -76.54564); //
    let latLng = new google.maps.LatLng(3.418428,-76.5376663); //Boxihome
 
    let mapOptions = {
      center: latLng,
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    
    this.addUser(latLng);
  }

  findMe(){

    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      
         let mapOptions = {
           center: latLng,
           zoom: 15,
           mapTypeId: google.maps.MapTypeId.ROADMAP,
           disableDefaultUI: true
         }
      
         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
         this.addUser(latLng);
    }, (err) => { this.loadMap(); } );

  }

  addUser(position){
    var image = 'http://maps.google.com/mapfiles/ms/micons/horsebackriding.png';
    this.user = new google.maps.Marker({
      position: position,
      map: this.map,
      icon: image
    });

    this.desde = this.user.getPosition().lat() + "," +this.user.getPosition().lng();
    this.updateUser();

    //Listeners del mapa
    this.map.addListener("click",(e)=>{

      let marker  = new google.maps.Marker({
        position: {lat:e.latLng.lat(),lng:e.latLng.lng()},
        map: this.map
        
      });
      this.hasta = marker.getPosition().lat() + "," +marker.getPosition().lng();
      
      console.log(e);
    });
  }

  updateUser(){

    
    setInterval(()=>{
      this.geolocation.getCurrentPosition().then((position) => {
        
              let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              //console.log("lat: "+latLng.lat+" , "+latLng.lng);
              this.user.setPosition(latLng);
              this.desde = this.user.getPosition().lat() + "," +this.user.getPosition().lng();

              if(this.fixcenter){
                this.map.setCenter(latLng);

              }
             

            }, (err) => { this.loadMap(); } );
    },1000);
/*
    var that = this;
    setInterval(function() {
    
    that.geolocation.getCurrentPosition().then((position) => {

      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      console.log("lat: "+latLng.lat+" , "+latLng.lng);
      that.user.setPosition(latLng);

    }, (err) => { that.loadMap(); } );
    }, 1000);
    */
  }

  calcularRuta(){

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(this.map);
    directionsService.route({
      origin: this.desde,
      destination: this.hasta,
      travelMode: 'DRIVING'
    }, function(response, status) {
        console.log(response);
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
          this.fixcenter=false;
       
       
        } else {
          window.alert('Directions request failed due to ' + status);
        }
    });


  }

  addMarker(animation){
    
    let effect: any;

    switch (animation) {
      case "DROP":
        effect = google.maps.Animation.DROP;
        break;
      case "BOUNCE":
        effect = google.maps.Animation.BOUNCE;
        break;
      default:
        effect = null;
        break;
    }

     let marker = new google.maps.Marker({
       map: this.map,
       animation: effect,
       position: this.map.getCenter()
     });
    
     let content = "<p>Le Marker!</p>";          
    
     let infoWindow = new google.maps.InfoWindow({
        content: content
      });
    
      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
      });
    
   }
}
