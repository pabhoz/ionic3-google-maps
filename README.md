# Instrucciones para el Ionic 3 + Google Maps

#1. Crear proyecto
creamos un nuevo proyecto en blanco de ionic 3 (yo uso el CLI v. 3.9.0)
`ionic start ionic3-maps blank`

#2. Agregar el SDK
Agregamos el SDK  de Google Maps a nuestra applicación, en cunato al SDK a usar se tiene la opción  del JS API para embeber Google Maps dentro de sitios web y el plugin de Cordova que funcionaría nativo y por ende tendría un mayor performance en el móvil.

JS API -> https://developers.google.com/maps/documentation/javascript/

Native (Cordova GoogleMaps API) -> https://github.com/mapsplugin/cordova-plugin-googlemaps

En este taller lo haremos con el JS  API para matar dos pajaros de un tiro aprendiendo a usar el API para sitios web.

###Agregar el API al root src/index.hmtl
```
<script src="http://maps.google.com/maps/api/js?key=YOUR_API_KEY_HERE"></script>
<script src="cordova.js"></script>
```
#3. Adicionarl Mapa

Adicionemos un mapa en la page por defecto "home"

En el contenido agregaremos un elemento para nuestro mapa (recordemos quitar el atributo padding del cuerpo para evitar incomodos espacios)

`<div #map id="map"></div>`

y antes de que pregunten '#map' nos permite obtener una referencia del elemento usando la anotación *ViewChild!* (si, como la de los menues)

#4. Adicionemos algo de lógica dentro del proyecto al home.ts

Importamos a nuestra page ViewChild y ElementRef para usar #map

 `import { Component, ViewChild, ElementRef } from '@angular/core';`

declaramos una variable google para evitar problemas en transpilación usando el API
a modo de FE.

`declare var google;`

luego creamos dos variables antes del constructor, una para la referencia, y otra para el mapa
```
  @ViewChild('map') mapElement: ElementRef;
  map: any;
```
Y ahora nos aseguramos que al cargar la vista se ejecute la carga del mapa:
```
ionViewDidLoad(){
    this.loadMap();
  }
 
  loadMap(){
 
    let latLng = new google.maps.LatLng(Latitud,Longitud);
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
  }
```
#5. Modificamos el estilo
```
.scroll {
    height: 100%;
}

#map {
    width: 100%;
    height: 100%;
}
```
Para que este estilo aplique a todo el app lo adicionamos en src/app/app.scss

#6. Test it up!

Para probar lo que hemos realizado vamos a correr el app tanto en web como en un emulador

`ionic serve`

###Para correr en el emulador de iOS se debe instalar el simulador dentro de *platforms/ios/codova*

  `sudo npm install ios-sim`

y posterior a ello podremos correr el proyecto emulado usando

  `ionic cordova emulate ios --livereload`

#6. Observad y maravillaos con el MAPA!

#7. Adicionar Geolocalización

`ionic cordova plugin add cordova-plugin-geolocation --save`

si no tienen ionic-native/gelocation instalado usar

`npm install --save @ionic-native/geolocation`

y agregarlo como proveedor en el app.module

```
  import { Geolocation } from '@ionic-native/geolocation';

  @NgModule({
  declarations: [
    ..
  ],
  imports: [
    ..
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ..
  ],
  providers: [
    ..
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
```

https://ionicframework.com/docs/native/geolocation/

#8. geolocation
Pasamos el objeto de geolocalización al constructor de nuestro home.ts y creamos un método para obtener nuestras coordenadas y cargar el mapa en nuestras coordenadas

`constructor(public navCtrl: NavController, public geolocation: Geolocation) {}`

y creamos un método muy similar al loadMap
```
  findMe(){

    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      
         let mapOptions = {
           center: latLng,
           zoom: 15,
           mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      
         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    }, (err) => err );

  }
```
En el emulador esto no servirá pues no hemos solicitado permisos para la gelocation así que modificamos findMe así:
```
findMe(){

    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      
         let mapOptions = {
           center: latLng,
           zoom: 15,
           mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      
         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    }, (err) => { this.loadMap(); } );

  }
```
#9. Los Marcadores!

escribimos una función que nos permita adicionar marcadores
```
addMarker(animation){
    
    let effect: any;

    switch (animation) {
      case "DROP":
        effect = google.maps.Animation.DROP;
        break;
      case "BOUNCE":
        effect = google.maps.Animation.BOUNCE;
        break;
      default:
        effect = null;
        break;
    }

     let marker = new google.maps.Marker({
       map: this.map,
       animation: effect,
       position: this.map.getCenter()
     });
    
     let content = "<p>Le Marker!</p>";          
    
     let infoWindow = new google.maps.InfoWindow({
        content: content
      });
    
      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
      });
    
   }
```
editamos los botones de home.html para agregar los marcadores 
```
<ion-buttons end>
      <button ion-button (click)="addMarker('DROP')"><ion-icon name="add"></ion-icon> Drop</button>
      <button ion-button (click)="addMarker('BOUNCE')"><ion-icon name="add"></ion-icon>Bounce</button>
</ion-buttons>
```
Ahora tenemos dos adicionadores de marcadores (drop y bounce)
